﻿# Calculadora de costos de comida
comida =  44.50 # Se declara la variable con el costo de la comida
impuesto = 0.0675 # Se declara la variable para el impuesto
propina = 0.15 # Se declara la variable para la propina
comida += comida * impuesto # Se suma el valor del impuesto al costo de la comida
total = comida + comida * propina # Se declara la variable para el total a pagar como resultado de agregar la propina al costo de la comida, después de impuestos
print('%.2f' % total) # Se imprime el resultado limitado a 2 decimales