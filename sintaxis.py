﻿# Variables
''' Una variable es una palabra/identificador que almacena un valor único '''
mi_variable = 10 # Una variable en Python se declara escribiendo el nombre seguido del signo = y el valor a almacenar

# Tipos de datos
''' Hay tres principales de valores: enteros, reales y booleanos '''
mi_ent = 7 # Entero
mi_real = 1.23 # Real
mi_bool = True # Booleano
mi_ent = 3 # Se reasigna el valor de la variable

# Sentencias
''' Las sentencias se identifican y delimitan por espacios en blanco, a diferencia de otros lenguajes donde es común el uso de ; '''
def jamon (): # Se declara una función
	huevos = 12 # Toda sentencia indentada que se encuentre debajo de la declaración de la función pasará a formar parte de esa función
	return huevos
print jamon() # Esa declaración no forma parte de la función, ya que no está indentada

# Interpretación
''' Python es un lenguaje interpretado que no precisa de un compilador para funcionar. Se lee y ejecuta directamente por un intérprete '''

# Comentarios
# Este es un comentario en Python
''' Esto es un docstring o comentario multilínea. Sirve para incluir más detalles acerca de una porción de código determinada '''

# Operadores airtméticos
''' Hay seis operadores aritméticos '''
# Suma (+)
big_var = 1 + 2
# Resta (-)
big_var = 5 - 2
# Multiplicación (*)
ni = 2 * 10
# División (/)
ni = 20 / 4
# Exponenciación (base ** exponente)
huevos = 10 ** 2
# Módulo (%)
huevos = 5 % 2