# Batalla naval
from random import randint # Se importa la función randint del módulo random
tablero = [] # Se declara una lista vacía para el tablero
for i in range(5): # Este bucle se ejecutará 5 veces. Al finalizar, el tablero contendrá 5 listas con 5 valores cada una
	tablero.append(['O'] * 5) # Cuando una lista se multiplica, se genera una nueva lista que contiene la misma lista repetida por el número de veces. En este caso se genera una lista con 5 elementos cuyo valor es 'O'. Esa lista se añade al tablero
def pinta_tablero (tablero): # Se declara una función que imprimirá el tablero recibido como parámetro
	for fila in tablero: # Este bucle se ejecutará una vez por cada lista dentro del tablero
		print ' '.join(fila) # Se imprime cada fila (lista) en una línea separada. El método join recibe como argumento una lista y concatena sus valores colocando entre ellos el caracter o string al cual se aplica el método
print '¡Juguemos batalla naval!' # Se da la bienvenida al juego
pinta_tablero(tablero) # Se imprime el tablero por primera vez
def fila_aleatoria(tablero): # Se declara una función que generará el número de fila en la que se ubicará el barco
	return randint(0, len(tablero) - 1) # Mediante el método randint, la función regresa un número aleatorio entero entre el 0 y la longitud del tablero menos 1, es decir 4
def columna_aleatoria(tablero): # Se declara una función que generará el número de columna en la que se ubicará el barco
	return randint(0, len(tablero[0]) - 1) # Mediante el método randint, la función regresa un número aleatorio entero entre el 0 y la longitud de la primera fila del tablero menos 1, es decir 4
barco_fila = fila_aleatoria(tablero) # Se declara la variable que almacenará el número de fila aleatoria
barco_columna = columna_aleatoria(tablero) # Se declara la variable que almacenará el número de columna aleatoria
print 'El barco se ubica en la fila %i' % (barco_fila) # Opcionalmente y con fines de depuración se imprime la ubicación del barco
print 'EL barco se ubica en la columna %i' % (barco_columna)
for turno in range(4): # Este bucle se ejecutará 4 veces, una por cada turno disponible para el jugador
	adivina_fila = input('Adivina en qué fila se encuentra en barco: ') # Mediante la función input se requiere la interacción del usuario para que adivine en qué fila se encuentra el barco
	adivina_columna = input('Adivina en qué columna se encuentra el barco: ') # Otro tanto sucede con la fila. La diferencia entre input() y raw_input() es que el primero evalúa la respuesta del usuario como una expresión, mientras que el segundo siempre toma la respuesta del usuario como un string
	if adivina_fila == barco_fila and adivina_columna == barco_columna: # Si el jugador ha adivinado la ubicación del barco...
		print '¡Felicitaciones! ¡Has hundido mi barco!' # Se notifica que el jugador ha ganado...
		break # ... y se rompe el bucle
	else: # Si el jugador falló se toma acción con base a varios casos
		if (adivina_fila < 0 or adivina_fila > len(tablero) - 1) or (adivina_columna < 0 or adivina_columna > len(tablero[0] - 1)): # Si la respuesta del usuario refleja una fila o columna fuera del rango del tablero...
			print 'Vaya, eso ni siquiera existe en el tablero' # ... se notifica que su respuesta está fuera de rango
		elif tablero[adivina_fila][adivina_columna] == 'X': # Si el jugador repite una respuesta...
			print 'Ya has dado esa respuesta' # ... se notifica que su respuesta ha sido repetida
		else: # En cualquier otro caso...
			tablero[adivina_fila][adivina_columna] = 'X' # ... se marca la casilla indicada por el usuario con una X...
			print '¡No impactaste mi barco!' # ... y se notifica al usuario que su respuesta es incorrecta
		print 'Turno: %i' % (turno + 1) # Se imprime el número de turno actual
		pinta_tablero(tablero) # Se imprime el tablero, ya con la(s) respuesta(s) incorrectas del usuario marcadas con una X
		if turno == 3: # Si el turno es el número 3 (4 en la práctica, ya que el for se comienza en 0)...
			print '¡El juego ha terminado :(' # ... se notifica al jugador que el juego ha terminado