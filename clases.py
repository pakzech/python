# Clases y objetos
''' Un objeto es una estructura sencilla que contiene datos y métodos (funciones). Una clase es una forma de producir objetos con métodos y atributos similares '''
string = 'ivan' # Esta variable es una instancia de la clase str
mi_dict = { 'mundo': 'cruel' } # Este diccionario es una instancia de la clase dict

# Sintaxis de una clase
''' La base para definir una clase es la instrucción class '''
class Animal (object): # Se coloca la instrucción class seguida del nombre de la clase (por convención debe comenzar con mayúscula); entre () se coloca la clase de la cual extiende; finalmente se colocan :
	pass # La instrucción pass no realiza ninguna acción. Sólo como reemplazo en las áreas de código en las cuales se espera una instrucción

# Función __init__()
''' Es necesaria en las clases y se usa para inicializar los objetos que crea. Por decirlo de otra forma, es la función que "arranca" cada objeto que crea la clase init es la abreviatura de initialize (inicializar) '''
class Animal (object): # Se crea una clase
	def __init__(self): # La instrucción __init__() siempre tiene al menos un argumento, self, que se refiere al objeto que se está creando
		pass
''' self es una convención ya que dicha palabra no tiene nada de especial, pero es supremamente común. Adicionalmente debería ser el primer parámetro
Python usará el primer argumento que se le pasa a __init__() para referirse al objeto que está siendo creado. self le da identidad al objeto '''

# Más parámetros en la función __init__()
''' Puede tomar otros parámetros para definir atributos del objeto '''
class Animal (object):
	def __init__(self, nombre, edad): # Se pasa un segundo argumento a la función
		self.nombre = nombre # Se indica que el atributo nombre del objeto debe ser igual al parámetro nombre recibido
		self.edad = edad # Se hace lo mismo para el atributo edad

# Instancia
''' Instanciar es crear un objeto a partir de una clase '''
cebra = Animal('Jeffrey') # Se crea un objeto de la clase animal y se envía su nombre y su edad como argumentos, mismos que estarán asociados con esta instancia en particula (notar que self se usa solamente en __init__() y no debe pasarse como argumento al instanciar)
print cebra.nombre # Para acceder a los atributos de un objeto se usa notación de puntos objeto.atributo

# Ámbito de clase
''' El ámbito de una variable es el contexto en el que es visible para el programa. Las partes de un programa no siempre tienen acceso a todas las variable
Cuando de usan clases se pueden tener variable que estén disponibles en todo lugar (variables globales); variables que sólo estén disponibles para los miembros de determinada clase (variables de miembro); o variables que sólo estén disponibles para determinadas instancias de una clase (variables de instancia)
Pasa lo mismo con las funciones '''
class Animal (object): # Se crea una clase
	esta_vivo = True # Esta es una variable de miembro. Todas las instancias de esta clase tendrán acceso a esta variable
	def __init__(self, nombre, edad): # Cada instancia tendrá su propio atributo nombre y edad, ya que todos fueron inicializados de forma individual
		self.nombre = nombre
		self.edad = edad
cebra = Animal('Jeffrey', 2) # Se crea una instancia de la clase Animal con nombre y edad propios
jirafa = Animal('Bruce', 1) # Se crea una instancia de la clase Animal con nombre y edad propios
print cebra.nombre # Imprime el valor del atributo nombre de esta instancia en particular
print jirafa.edad # Imprime el valor del atributo edad de esta instancia en particular
print cebra.esta_vivo # Imprime True, que es el valor del atributo esta_vivo para toda la clase
print jirafa.esta_vivo  # Imprime True, que es el valor del atributo esta_vivo para toda la clase

# Métodos
''' Una función asociada a una clase es llamada método '''
class Animal (object): # Se crea una clase
	esta_vivo = True # Esta es una variable de miembros. Las variables de miembros están disponibles para todos los métodos de la clase
	def __init__(self, nombre, edad): # __init__() es el método principal de una clase por ser el inicializador
		self.nombre = nombre
		self.edad = edad
	def descripcion(self): # Se define un método personalizado para la clase. De forma implícita recibe también el parámetro self (identificador del objeto)
		print self.nombre, self.edad
hipopotamo = Animal('Edith', 24) # Se crea una nueva instancia de la clase
hipopotamo.descripcion() # Se llama al método del objeto

# Herencia
''' La herencia es un proceso en el cual una clase toma los atributos y métodos de otra, es decir, extiende de otra clase
En un caso hipotético, la clase Panda ser herencia de la Clase Oso; pero la Clase Toyota no podría ser herencia de la clase Tractor, a pesar de que ambas compartan muchas cosas en común, así que en este caso lo más lógico es que ambas clases extendieran de otra llamada Vehiculo
La sintaxis de una herencia funcioná así: class ClaseDerivada (ClaseBase), donde la clase derivada es la nueva clase que se está creando y la clase base es la clase desde la cual hereda la nueva clase '''
class Figura (object): # Se crea una clase base
	def __init__(self, lados):
		self.lados = lados
class Triangulo (Figura): # Se crea una clase derivada que extiende de la clase Figura, lo cual está indicado entre ()
	def __init__(self, lado1, lado2, lado2):
		pass

# Sustitución
''' Cuando un método o atributo se redefine dentro de una subclase, éste se sobreescribe dentro de la clase derivada '''
class Empleado (object): # Se crea una clase base
	def __init__(self, nombre):
		self.nombre = nombre
	def calcular_sueldo(self, horas): # Se define un método
		return horas * 20
class EmpleadoMedioTiempo (Empleado): # Se crea una subclase
	def calcular_sueldo(self, horas): # Se define un método que sobreescribe al de la clase base para las instancias que extiendan desde esta clase derivada
		return horas * 10

# Función super()
''' Después de sobreescribir un método sigue siendo posible acceder al método original de la clase padre (superclase), mediante el método super() '''
class EmpleadoMedioTiempo (Empleado): # Se crea una subclase
	def calcular_sueldo(self, horas): # Se define un método que sobreescribe al de la clase base para las instancias que extiendan desde esta clase derivada
		return horas * 10
	def calcular_sueldo_completo(self, horas): # Se define un nuevo método mediante el cual se accederá al método sobreescrito original de la superclase
		return super(EmpleadoMedioTiempo, self).calcular_sueldo(horas) # La sintaxis de la función super() es recibir dos argumentos, el primero el nombre de la subclase, segundo self; luego desde allí se llama al método orginal de la superclase