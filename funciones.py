﻿# Funciones
''' Una función es una porción de código reusable para resolver una determinada tarea '''

# El Zen de Python
''' Un huevo de pascua que explica la filosofía de Python '''
import this

# Sintaxis
''' Las funciones tienen tres partes: encabezado, docstring o string de documentación, cuerpo '''
def jamon (): # El encabezado incluye la instrucción def seguida del nombre de la función, los argumentos y :
	''' Esta es una función de demostración que servirá para identificar su estructura básica ''' # El docstring incluye información útil acerca de una función
	print '¡Huevos!' # El cuerpo de la función es el código a ejecutar
jamon() # De esta forma se llama a la función

# Argumentos
'''Los argumentos son las porciones de código que se colocan entre () al momento de llamar una función. El parámetro es el nombre que se escribe entre () al momento de definir la función '''
def potencia (base, exponente): # Se declara la función y se definen los parámetros a recibir. Los parámetros son alias que la función da a los argumentos recibidos
	resultado = base ** exponente
	print '%d a la %d potencia es %d' % (base, exponente, resultado)
potencia(37, 4) # Se llama a la función pasando dos argumentos, uno correspondiente al parámetro de base y otro al parámetro de exponente, es ese orden

# Argumentos múltiples
''' Los argumentos múltiples son la opción correcta cuando no se sabe exactamente cuántos argumentos recibirá la función al ser llamada '''
def actores_favoritos (*args): # Al declarar la función se usa un * seguido de nombre de los parámetros. Por convencionalismo se usa *args aunque puede ser cualquier nombre siempre que se anteponga un *
	print 'Tus actores favoritos son' , args
actores_favoritos('Michael Palin', 'John Cleese', 'Graham Chapman') # Al llamar a la función se envían tres argumentos

# Llamar funciones desde otra función
def amor_con_amor (n): # Se declara una función
	return n + 1
def se_paga (n): # Se declara una nueva función...
	return amor_con_amor(n) + 2 # ... que regresa el resultado de llamar a la función anterior + 2

# Importación genérica
''' La instrucción import sirve para importar módulos. Un módulo es un archivo que contiene definiciones que se pueden usar, lo que incluye variables y funciones
El intérprete de Python se atascaría si tuviera variables y funciones presentes todo el tiempo, así que sólo tiene que importarse un módulo cuando se necesite '''
print sqrt(25) # Arroja un error, ya que sqrt no está definido
import math # Se importa el módulo math de Python
print math.sqrt(25) # Se llama a la función sqrt del módulo math

# Importación de función
''' La importación genérica puede ser molesta, ya que importa un módulo completo del que probablemente se requiera sólo una función, adicional al inconveniente tener que usar siempre la notación modulo.funcion() para recuperar la función en cuestión
Es posible importar sólo ciertas variables o funciones de un módulo determinado, a esto se le llama importación de función, misma que se realiza con la instrucción from '''
from math import sqrt # Desde el módulo math, importa únicamente la función sqrt
print sqrt(25) # Ahora ya no tiene que usarse la notación modulo.funcion() para recuperar la función

# Importaciones universales
''' Al realizar una importación genérica, se pueden utilizar todos los elementos dentro del módulo, pero eso elementos siguen estando dentro del módulo, por lo que al referenciar a uno de ellos se debe usar la notación modulo.elementos
Al realizar una importación de función, se está importando un sólo elemento, por lo que si se requiere usar un elemento adicional de ese módulo se tiene que importar por separado
Por otro lado, la importación universal permite importar todos los elementos de un módulo sin depender del contenedor, por lo que no requiere el uso de la notación modulo.elemento. Sin embargo este método es mala idea, ya que se puede saturar el programa o incurrir un conflictos de nombres y en desorganización '''
from math import * # Desde el módulo math, importa todos los elementos
print sqrt(25) # Se puede recuperar la función sin hacer referencia al módulo

# Listar todos los elementos de un módulo
dir(math) # La función dir muestra todos los elementos disponibles en un determinado módulo

# Funciones incorporadas
''' Existen funciones incorporada de forma nativa que no requieren importar módulos '''
# max(). Toma cualquier cantidad de elementos (enteros o reales) y regresa el mayor de ellos
max(1, 2, 3, 3.1) # Regresará 3.1
# min(). Lo mismo que max(), pero regresa el menor
min(-1, 0, 1.5) # Regresará -1
# abs(). Toma un solo argumento numérico y regresa el valor absoluto. EL valor absoluto es la distancia entre ese número y el 0, independientemente de que el valor sea negativo
abs(-42) # Regresará 42
# type(). Regresa el tipo de datos del argumento recibido
type(42) # Regresa <type 'int'>. Entero
type(4.2) # Regresa <type 'float'>. Real
type('hola') # Regresa <type 'str'>. String
type({ 'Nombre': 'John Cleese' }) # Regresa <type 'dict'>. Diccionario
type((1, 2)) # Regresa <type 'tuple'>. Tupla

# Ejercicio
def distancia_desde_cero (n): # Función que recibe el parámetro n
	if type(n) == float or type(n) == int: # Si el tipo de datos de n es igual a float o int (real o entero)...
		return abs(n) # ... regresa el valor absoluto de n
	else: # De otra forma...
		return '¡Esto no es un número!' # ... indica que el valor recibido no es numérico