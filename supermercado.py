﻿# Supermercado
precios = { # Se declara un diccionario para los precios
	'banano': 4, # Cada par contiene el producto y su precio unitario
	'manzana': 2,
	'naranja': 1.5,
	'pera': 3
}
inventario = { # Se declara un diccionario para el inventario
	'banano': 6, # Cada para contiene el producto y sus existencias en unidades
	'manzana': 0,
	'naranja': 32,
	'pera': 15
}
for i in precios: # Este bucle for itera sobre cada producto que se encuentre en la lista de precios...
	print i # ... imprime el nombre del art??culo...
	print 'precio: %i' % (precios[i]) # ... imprime su precio...
	print 'inventario: %i' % (inventario[i]) # ... imprime sus existencias
venta_total = 0 # De declara una variable para almacenar el valor del total posible de la venta (en caso de vender todas las unidades de todos los art??culos)
for i in precios: # Este bucle for itera sobre cada producto que se encuentre en la lista de precios...
	venta_total += precios[i] * inventario[i] # ... suma al total de la venta el resultado de multiplicar el precio del art??culo por sus existencias
print venta_total # Se imprime el valor total posible de la venta
def calcularFactura (compras): # Se define una funci??n para manejar las compras. Se espera una lista como par??metro
	total = 0 # El total inicial de la compra es de 0
	for i in comida: # Se itera sobre la lista recibida...
		if inventario[i] > 0: # ... y si el art??culo est?? en el inventario...
			total += precios[i] # ... se suma al total de la compra su precio unitario...
			inventario[i] -= 1 # ... y se resta la unidad del inventario
	return total # La funci??n regresa el total de la compra
print calcularFactura(['banano', 'naranja', 'manzana']) # Se imprime el resultado de llamar a la funci??n y pasar una lista de los art??culos a comprar