# Pares clave / valor de un diccionario
''' Mediante el método items() puede obtenerse una lista cuyos elementos consistirán en pares clave / valor de un diccionario '''
mi_dict = { 'hola': 'adiós', 'mundo': 'cruel', 'sombrero': 'dopado' } # Se declara un diccionario
mi_dict.items() # Regresa una lista con los pares clave / valor agrupados por tuplas (listas inmutables delimitadas por () en lugar de []) del diccionario

# Claves y valores de un diccionario
''' Pueden obtenerse listas que contengan sólo las claves o sólo los valores de un diccionario. '''
mi_dict = { 'hola': 'adiós', 'mundo': 'cruel', 'sombrero': 'dopado' } # Se declara un diccionario
mi_dict.keys() # Regresa una lista con todas las claves del diccionario
mi_dict.values() # Regresa una lista con todos los valores del diccionario
''' Los métodos items(), keys() y values() regresan sus respectivas listas en un orden arbitrario pero no aleatorio '''

# El operador in
''' El operador in es usado para iterar sobre listas, tuplas, diccionarios, strings y series '''
for numero in range(5): # Itera por cada número en un serie
	print numero
d = { 'nombre': 'Ivan', 'edad': 26 }
for clave in d: # Itera por cada clave en un diccionario
	print clave
	print d[clave]
for elemento in [1, 2, 3, 'hola']: # Itera por cada elemento en una lista
	print elemento
for elemento in (1, 2, 3, 'hola'): # Itera por cada elemento en una tupla
	print elemento
for letra in 'Ivan': # Itera por cada caracter en un string
	print letra

# Creación de listas en serie
''' Es posible crear listas a partir de series range() '''
mi_lista = range(51) # Crea una lista con los números del 0 al 50
mi_lista = range(0, 51, 2) # Crea una lista con todos los números pares del 0 al 50

# Comprensión de listas
''' Las comprensiones de lista son una forma de generar listas usando for / in e if '''
events_to_50 = [i for i in range(0, 51) if i % 2 == 0] # Se crea una lista en la que se irá añadiendo el valor de i al iterar en la serie 0 a 51 si i es un número par
dobles = [i * 2 for i in range(0, 6)] # Se crea una lista en la que se irá añadiendo el valor de i multiplicado por 2 al iterar en la serie 0 a 6
dobles_por_3 = [i * 2 for i in range(1, 6) if (i * 2) % 3 == 0] # Se crea una lista en la que se irá añadiendo el valor de i multiplicado por 2 al iterar en la serie 1 a 6 si i multiplicado por 2 es divisible entre 3

# Particionado de listas
''' El particionado de listas se realiza con una notación de corechetes cuya lógica es [start:end:stride]. Los valores predeterminados son el elemento inicial, el elemento final y 1 '''
lst = [i ** 2 for i in range(1, 11)] # Se crea una lista [1, 4, 9, 16, 25, 36, 49, 64, 81, 100]
print lst[2:9:2] # Imprime una partición de lst comenzando en el índice 2, terminando en el 9 (exclusive) y con un salto de 2
print lst[::2] # Imprime una partición de lst comenzando con el primer elemento, terminando en el último y con un salto de 2
print lst[::-1] # Un paso positivo recorre la lista de izquierda a derecha, uno negativo la recore de derecha a izquierda. En este caso se imprime la lista completa invertida

# Lambdas
''' Las lambdas son funciones anónimas que se definen al momento de enviarse como argumentos de una función '''
mi_lista = range(16) # Se declara una lista con los números del 0 al 15
filter(lambda x: x % 3 == 0, mi_lista) # La función filter recibe dos argumentos. En el primer agumento se envía una lambda, es decir, se define una función que recibe un parámetro x y regresa x % 3 == 0; el segundo argumento es la lista a filtrar

# Sintaxis de lambda
''' La sintaxis de una lambda es: lambda variable: expression. Son últiles cuando se requiere una función rápida que resuelva algo '''
cuadrados = [i ** 2 for i in range(1, 11)]
print filter(lambda i: i >= 30 and i <= 70, cuadrados) # Se comienza con lambda, seguido de i que es la variable tratada como parámetro, después de : viene la expresión que compone la función

# Expresiones condicionales
''' En otros lenguajes llamados operadores ternarios '''
mi_variable = 1 if 30 % 2 == 0 else 0 # La sintáxis de una expresión condicional es: primero se coloca el valor predeterminado; luego viene la condición if a evaluar para aplicar el valor predeterminado; finalmente else seguido del valor secundario a aplicar si la condición evalúa como False