﻿# Strings
''' Los strings son cadenas de caracteres que pueden incluir letras, números u otros símbolos '''
brian = '¡Mira siempre el lado positivo de la vida' # Este es un string literal declarado de forma explícita. Los strings se escriben entre comillas simples o dobles

# Escape
''' Dentro de un string no pueden incluirse todos los síbmolos. Algunos, como las propias comillas, requieren estar escapados con una barra invertida ya que de otra manera provocan errores '''
help = 'Help! I\'m being represed!' # El apóstrofe está espapado con una \. De otra forma cortaría el string en ese punto

# Acceso por desplazamiento
''' Cada caracter en un string tiene un índice o desplazamiento. El índice del primer caracter es 0 y es incremental en los sucesivos '''

# Métodos de string
''' Un método es una función asociada a un objeto. Los strings tienen métodos predeterminados listos para ser usados '''
# len(). Regresa la longitud de caracteres de un string
loro = 'Azul Noruego'
print len('loro')
# lower(). Convierte un string a minúsculas
print loro.lower()
# upper(). Convierte un string a mayúsculas
print loro.upper()
# str(). Convierte a string cualquier otro tipo de dato
pi = 3.14159
print str(pi)
''' Los métodos lower() y upper() se usan con notación de puntos aplicados a strings literales o a variables que almacenan strings. Los métodos len() y str() no son específicos de strings por lo que no se usan con notación de puntos '''
monty = 'PYTHON'[4] # Esta variable almacena el 5° caracter del string
''' El string PYTHON tiene seis caracteres numerados del 0 al 5 de la siguiente forma:
+---+---+---+---+---+---+
| P | Y | T | H | O | N |
+---+---+---+---+---+---+
0   1   2   3   4   5
Pese a poder acceder al índice de un caracter de un string, éste no puede cambiarse ya que en Python todos los strings son inmutables; no pueden modificarse una vez creados '''

# print
''' print muestra el resultado de una evaluación que el intérprete hace de una porción de código '''
print 'Monty Python'
la_maquina_hace = '¡Ping!'
print la_maquina_hace

# Concatenación
''' Concatenar significa unir dos valores para formar un solo string. En Python la concatenación se realiza con el operador aritmético + '''
print 'Spam ' + '\'n ' + 'eggs'

# Conversión explícita de strings
''' Es el proceso de convertir implícitamente un valor que no es un string, en un string '''
print str(3.14)

# Formateo de string con %
''' Se puede formatear un string para concatenar valores de una forma más limpia y flexible '''
camelot = 'Camelot' # Una variable que almacena un string
lugar = 'lugar' # Otra variable que almacena un string
print 'No vayamos a %s. Es un %s tonto' % (camelot, lugar) # Se imprime un string que incluye dos veces el símbolo %s. Al cerrar el string se coloca % segudio de la variable o variables (también pueden ser valores, como strings literales) separadas por comas y encerradas entre () que ocuparan en el orden respectivo los lugares de los símbolos %s
nombre = raw_input('¿Cuál estu nombre?') # El método raw_input() requiere interacción y respuesta del usuario
mision = raw_input('¿Cuál es tu misión?')
color = raw_input('¿Cuál es tu color favorito')
print 'Así que tu nombre es %s. Tu misión es %s y tu color favorito es el %s' % (nombre, mision, color)