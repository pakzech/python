﻿# Calcular el gasto del hotel
def costoHotel (dias):
	''' Calcula el costo del hotel. El costo por día es de $140 '''
	return dias * 140

def costoAvion (ciudad):
	''' Calcula el costo del avión (variable de acuerdo a la ciudad) '''
	if ciudad == 'Tampa':
		return 183
	elif ciudad == 'Los Angeles':
		return 475
	elif ciudad == 'Seattle':
		return 400

def costoAuto (dias):
	''' Calcula el costo de alquilar un auto. El costo por día es de $40. Si la renta es por más de 3 días, se hace un descuento de $20; o un descuento de $50 por 7 días o más '''
	costo = dias * 40
	if dias >= 7:
		costo -= 50
	elif dias >= 3:
		costo -= 20
	return costo

def costoViaje (ciudad, dias, gastosAdicionales):
	''' Calcula el costo total de las vacaciones de acuerdo a la ciudad y días y gastos adicionales '''
	return costoHotel(dias) + costoAvion(ciudad) + costoAuto(dias) + gastosAdicionales

print costoViaje ('Los Angeles', 5, 600) # Se imprime el costo de unas vacaciones Los Angeles por 5 días y gastos adicionles por $600