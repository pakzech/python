# Bucle while
''' Es similar a la sentencia if. Ejecuta el código indicado dentro del mismo si una condición es verdadera. La diferencia del bucle while es que éste seguira ejecutándose mientras la condicional siga siendo verdadera '''
recuento = 0 # Se declara la variable recuento inicializada en 0
if recuento < 5: # La sentencia if evalúa una expresión y si es verdadera...
	print 'Hola, soy una sentencia if y el recuento es ', recuento # ... ejecuta una porción de código
while recuento < 5: # El bucle while evalúa una expresión y si es verdadera...
	print 'Hola, soy un bucle while y el recuento es ', recuento # ... ejecutará una porción de código hasta que la expresión deje de evaluar como verdadera
	recuento += 1 # En este caso, con cada ejecución el valor de recuento se incrementará en 1, dee sta forma en algún momento la expresión evaluada por el bucle dejará de ser verdadera

# Condición
''' La condición es la expresión que decide si un bucle se va a ejecutar o no '''
condicion_bucle = True # 1. La variable se fija en True
while condicion_bucle: # 2. El bucle verifica si la expresión es verdadera. Si es así, entra el bucle
	pass # 3. Se ejecuta el bucle
	condicion_bucle = False # 4. La variable se fija en False. 5. El bucle verifica de nuevo que la expresión sea verdadera y si ya no lo es no se ejecutará de nuevo

# Aplicaciones prácticas
''' Ejecutar un código hasta esperar una respuesta conocida '''
choice = raw_input('¿Estás disfrutando de este curso? (s/n): ') # Se requiere una respuesta del usuario s o n
while choice != 's' and 'choice' != n: # Si la respuesta no es s ni n...
	choice = 'Lo siento, no entendí eso' # ... se continúa solicitando una respuesta hasta que la misma sea conocida

# while infinito
''' Un bucle infinito es un bucle que nunca sale. Son peligrosos ya que hacen que un programa se cuelgue '''
while 1 != 2: # La lógica de este bucle nunca permitirá que  la condición sea falsa, por lo que se ejecutará eternamente
	pass
recuento = 10 # Se declara la variable recuento inicializada en 10
while recuento > 0: # La lógica de este bucle es correcta...
	pass
	recuento += 1 # ... pero se incrementa la variable recuento en 1 en lugar de decrementarla. Por esta razón la condición nunca evaluará como falsa y el bucle se ejecutará eternamente

# break
''' Es una sentencia de una sola línea que quiere decir "salir del bucle actual" '''
recuento = 0 # Se declara la variable recuento inicializada en 0
while True: # Este bucle se ejecutará siempre
	print recuento # Imprime el valor de recuento
	recuento += 1 # Incrementa el valor de recuento en 1
	if recuento >= 10: # La condición se evalúa al final con una sentencia if. Cuando esta condición deje de avaluar como verdadera...
		break # ... entra el break y el bucle se detiene

# while / else
''' while / else es parecido a if / else. El bloque else se ejecutará cada vez que la condición del bucle sea falsa, por ejemplo si nunca se ingresa al bucle o si se sale normalmente, pero no se ejecutará si el bucle se detiene como resultado de un break '''
from random import randrange # randrange() recibe tres argumentos y genera un número aleatorio entre el valor del primero y el segundo, en rangos del tercero
recuento = 0 # Se declara la variable recuento inicializada en 0
while  recuento < 3: # Este bucle se ejecutará mientras recuento sea menor a 3
	num = randrange(2, 20, 3) # Se genera un número aleatorio entre 2 y 20 y en rangos de 3 y se almacena en la variable num
	print num # Se imprime num
	recuento += 1 # Se incrementa en 1 recuento
	if num == 5: # Si num es igual a 5...
		print 'Tú pierdes'
		break # ... entra el break y el bucle se detiene
else: # Si el bucle nunca se detuvo como resultado de un break y la condición ha evaluado como falsa...
	print '¡Tú ganas!' # ... entra el else

# Bucle for
''' El bucle for trabaja en series '''
for i in range(0, 10): # Por cada número i en la serie 0 a 9...
	print i # ... ejecuta este código

# Ejecutar un código cierto número de veces
pasatiempos = [] # Se declara una lista vacía
for i in range(3): # El búcle se ejecutará 3 veces...
	pasatiempos.append(input('Escribe un pasatiempo: ')) # ... y por cada una agregará un elemento a la lista

# Iterar en strings
palabra = '¡spam!' # Se declara una variable con un string
for caracter in palabra: # El bucle se ejecutará una vez por cada caracter en el string
	print caracter

# Recorrer listas
''' El uso más práctico y común de un bucle for es iterar sobre una lista '''
numeros = [7, 9, 18, 87] # Se declara una lista
for i in numeros: # El bucle itera en cada elemento de la lista
	print i ** 2

# Recorrer diccionarios
''' Cuando se itera sobre un diccionario, se itera sobre la clave, pero el valor puede obtenerse muy fácilmente '''
d = {'x': 9, 'y': 10, 'z': 20} # Se declara un diccionario
for clave in d: # El bucle itera en cada clave del diccionario
	print clave # Se accede a la clave
	print d[clave] # Se accede al valor

# Contar e iterar
''' enumerate() recorre una lista y la enumera a partir de 0 o de un número elegigo '''
choices = ['pizza'. 'pasta', 'ensalada', 'nachos'] # Se declara una lista
list(enumerate(choices)) # Regresa una lista numerada
list(enumerate(choices, 1)) # Regresa una lista numerada a partir de 1
for i, v in enumerate(choices, 1): # Este bucle itera sobre cada índice y valor en una lista numerada a partir de 1
	print i # Imprime el número asignado al elemento
	print v # Imprime el valor del elemento

# Listas múltiples
a = [3, 9, 17, 15, 19] # Se declara una lista
b = [2, 4, 6, 8, 10, 20, 30, 40, 50, 60, 70, 80, 90] # Se declara una lista
c = zip(a, b) # zip() toma dos listas y crea una nueva lista realizando pares, el primer elemento de la lista contiene el primer elemento de cada una de las listas, el segundo elemento de la nueva lista contiene el segundo elemento de cada una de las listas, y así sucesivamente deteniéndose en el momento que termina la lista más correcta
a2, b2 = zip(*c) # zip() cuando recibe primero el operador * seguido de una lista, divide dicha lista (previamente "zipeada") en dos
for i, j in zip(a, b): # Este bucle itera sobre el primer y segundo valor de cada elemento de una lista "zipeada"
	print max(a, b) # Se imprime el número mayor

# for / else
''' Al igual que un bucle while, un for puede tener un else asociado que se ejecutará cuando el bucle termine, siempre que no se termine como resultado de un break '''
frutas = ['banano', 'manzana', 'naranja', 'tomate', 'pera', 'uva'] # Se declara una lista
for fruta in frutas: # Se itera sobre cada elemento de la lista
	if fruta == 'tomate': # Si esta condición evalúa como verdadera...
		print '¡El tomate no es una fruta!'
		break # ... entra el break y el bucle termina
	else:
		print fruta
else: # Este es el código a ejecutar cuando el bucle termine, pero si el bucle termina con un break, el else no entrará
	print 'Una selección exclusiva de frutas'