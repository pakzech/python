﻿# Los estudiantes
Lloyd = { # Se declara un diccionario por cada estudiante
	'nombre': 'Lloyd', # El nombre del estudiante
	'tareas': [90, 97, 75, 92], # Una lista con la calificación de 4 tareas
	'pruebas': [88, 40, 94], # Una lista con la calificación de 3 pruebas
	'exámenes': [75, 90] # Una lista con la calificación de 2 exámenes
}
Alice = {
	'nombre': 'Alice',
	'tareas': [100, 92, 98, 100],
	'pruebas': [82, 83, 91],
	'exámenes': [89, 97]
}
Tyler = {
	'nombre': 'Tyler',
	'tareas': [0, 87, 75, 22],
	'pruebas': [0, 75, 78],
	'exámenes': [100, 100]
}
estudiantes = [Lloyd, Alice, Tyler] # De declara una lista que contiene a todos los estudiantes
def promedio (calificaciones): # Se declara una función que recibe como parámetro una lista correspondiente a un bloque de tareas, pruebas o exámenes
	return sum(calificaciones) / len(calificaciones) # La función sum() suma todos los valores de la lista y los divide entre el número de elementos. De esta forma se obtiene el promedio del bloque
def calcularPromedio (estudiante): # Se declara una función que recibe como parámetro un diccionario correspondiente a un estudiante
	return promedio(estudiante['tareas']) * 0.10 + promedio(estudiante['pruebas']) * 0.30 + promedio(estudiante['exámenes']) * 0.60 # Se obtiene el promedio ponderado por alumno. Las tareas constituyen el 10% de la calificación global, las pruebas el 40% y los exámenes el 60%
def calificacionEnLetra (nota): # Se declara una función que recibe como argumento una nota y regres la calificación en letra
	nota = round(nota) # Mediante la función round() se redondea el valor recibido
	if nota < 60: # Si la nota es menor a 60...
		return 'F' # ...  la calificación obtenida es F
	elif nota < 70: # Si la nota es menor a 70...
		return 'D' # ...  la calificación obtenida es D
	elif nota < 80: # Si la nota es menor a 80...
		return 'C' # ...  la calificación obtenida es C
	elif nota < 90: # Si la nota es menor a 90...
		return 'B' # ...  la calificación obtenida es B
	else: # De otra manera (si es mayor a 90)...
		return 'A' # ... la calificación obtenida es A
def promedioClase (clase): # Se declara una función que recibe como parámetro una lista correspondiente a una clase
	promedios = [] # Se declara una lista vacía en la que posteriormente se almacenarán los promedios finales de cada estudiante
	for estudiante in clase: # Se itera por cada estudiante en la lista
		promedios.append(calcularPromedio(estudiante)) # Se guarda en la lista de promedios el promedio del estudiante
	return promedio(promedios) # La función regresa el promedio de los promedios, es decir, el promedio general de la clase
print promedioClase(estudiantes) # Se imprime el promedio de la clase