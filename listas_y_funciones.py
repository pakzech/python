﻿# Pasar una lista a una función
''' Las listas se pasan a una función como cualquier otro argumento '''
n = [1, 2, 3] # Se declara una lista
def myFunc (lst): # Se declara una función que recibe una lista
	print lst[1] # Se puede tomar sólo uno o varios elementos de la lista para usarlos dentro de una función...
	lst[0] += 3 # ... modificar elementos individuales...
	lst.append(9) # ... agregar elementos...
	return lst # ... o trabajar con la lista completa
myFunc(n) # Se llama a la función pasando la lista como argumento

# Imprimir elementos individuales
''' Función para imprimir elementos de una lista de forma individual '''
def myFun (lst): # Se declara una función que recibe una lista
	for i in lst: # Se itera sobre la lista...
		print i # y se imprime cada elemento de forma individual

# Modificar elementos
''' La itreación sobre listas, para luego acceder a sus índices de forma individual se hace de una forma distinta '''
n = [3, 5, 7] # Se declara una lista
def myFun (lst): # Se declara una función que recibe una lista
	for i in range(0, len(lst)): # Se itera mediante un for por cada elemento de la lista
		lst[1] *= 2 # Se modifica individualmente cada elemento de la lista
	return lst
''' La iteración mediante for siguiendo la lógica "for elemento in lista" regresa o itera sobre el elemento propiamente dicho. Es decir, siguiendo este procedimiento...
lst = [3, 5, 7]
for i in lst: <===> Se itera sobre el elemento de la lista
	print i <===> Imprimiría de forma individual 3, 5 y 7
La iteración mediante for siguiendo la lógica "for índice in range(0, tamaño de la lista)" regresa o itera sobre números, no sobre el elemento como tal. Es decir, siguiendo este procedimiento...
lst = [3, 5, 7]
for i in range(0, len(lst)): <===> Se itera sobre números comenzando en 0 y terminando en 3 (el tamaño de la lista)
	print i <===> Imprimiría de forma individual 0, 1 y 2
	print lst[i] <===> Imprimiría individualmente los valores de los índices en lst, es decir lst[0], lst[1] y lst[2] '''

# Series
''' Las series determinadas por range() forman listas '''
n = range(1) # Regresa [0]. Si se usa un solo algumento, se genera una lista, comenzando por 0, incrementando en 1 hasta llegar al valor del argumento recibido
n = range(2) # Regresa [0, 1]
n = range(0) # Regresa [], una lista vacía
n = range(1, 3) # Regresa [1, 2]. Si se usan dos argumentos, se genera una lista, comenzando por el valor del pirmer argumento, incrementando en 1 hasta llegar al valor del segundo argumento
n = range(2, 8, 3) # Regresa [2, 5]. Si se usan tres argumentos, se genera una lista, comenzando por el valor del primer argumento, incrementando en el valor del tercer argumento hasta llegar al valor del segundo argumento
n = range(2, 9, 3) # Regresa [2, 5, 8]

# Sumar listas en una función
''' Cuando todos los valores de una lista son numéricos, pueden sumarse con la función sum() '''
n = [2, 5, 8] # Se declara una lista
def myFun (lst): # Se declara una función que recibe una lista
	return sum(lst) # sum() regresa la suma de todos los valores recibidos, en este caso todos los valores de la lista

# Usar dos listas en una función
''' Usar más de una lista en una función es tan sencillo como pasarlas como argumentos '''
m = [1, 2, 3] # Se declara una lista
n = [4, 5, 6] # Se declara una lista
def myFun (lst1, lst2): # Se declara una función que recibe dos listas
	return lst1 + lst2 # Se anexa la segunda lista a la primera
myFun(n, m) # Se llama a la función pasando las dos listas. La función regresa ambas listas contenidas en una sola

# Usar un número arbitrario de listas en una función
''' Recibir un número arbitario de listas en una función es exactamente igual que recibir un número arbitrario de parámetros '''
m = [1, 2, 3] # Se declara una lista
n = [4, 5, 6] # Se declara una lista
o = [7, 8, 9] # Se declara una lista
def myFun (*lists): # Se declara una función que recibe un número indeterminado de listas
	x = [] # Se declara una lista vacía
	for lst in lists: # Se itera por cada lista recibida
		x += lst # Se anexa la lista a la lista nueva
	return x
myFun(m, n, o) # Se llama a la función pasando tres listas como argumento

# Usar una lista de listas
n = [[1, 2, 3], [4, 5, 6], [7, 8, 9]] # Se declara una lista que contiene tres listas
def myFun (lst): # Se declara una función que recibe una lista
	x = [] # Se declara una lista vacía
	for subLst in lst: # Se itera sobre cada sublista en la lista recibida
		x += subLst # Se anexa la sublista a la lista nueva
	return x
myFun(n) # Se llama a la función pasando la lista