﻿# Control de flujo
''' El control de flujo permite tener resultados múltiples derivados de la interacción del usuario o de una variedad de factores en el entorno (todas las variables y sus valores) y tomar acción en consecuencia '''
def clinic():
	print '¡Acabas de entrar en la clínica!'
	print '¿Vas a ir por la puerta de la izquierda o de la derecha?'
	answer = raw_input('Escribe izquierda o derecha y Presiona "Enter":')
	if answer.lower() == 'izquierda':
		print '¡Este es el cuarto de abuso verbal, montón de estiércol de pájaro!'
	elif answer.lower() == 'derecha':
		print '¡Por supuesto que este es el cuarto de discusiones, sí ya se lo dije!'
	else:
		'No escogiste ni izquierda ni derecha. Inténtalo de nuevo'
		clinic()
clinic()

# Comparadores
''' Existen seis comparadores básicos '''
# Igual a (==). En Python no existe ===. = se usa para asignar un valor. == se usa para comparar valores
100 == 33 * 3 + 1 # Regresa True
# No es igual a (!=)
99 != 98 + 1 # Regresa False
# Menor que (<)
17 < 118 % 100 # Regresa True
# Menor o igual que (<=)
19 <= 2 ** 4 # Regresa False
# Mayor que (>)
50 > 49 # Regresa True
# Mayor o igual que (>=)
-22 >= -18 # Regresa False

# Comparadores booleanos
''' Los operadores booleanos u operadores lógicos son  palabras que sirven para conectar sentencias de forma gramaticalmente correcta. Existen tres comparadores booleanos '''
# and. Significa "y". Sólo regresa True cuando las expresiones en ambos lados son True
-(-(-(-2))) == -2 and 4 >= 16 ** 0.5 # Regresa False
-(1 ** 2) < 2 ** 0 and 10 % 10 <= 20 - 10 * 2 # Regresa True
# or. Significa "o" y quiere decir "una cosa, o la otra, o ambas". Sólo regresa False cuando las expresiones en ambos lados son False
2 ** 3 == 108 % 100 or 'Cleese' == 'King Arthur' # Regresa True
1 ** 100 == 100 ** 1 or 3 * 2 * 1 != 3 + 2 + 1 # Regresa False
# not. Significa "no". Da como resultado True para sentencias False y False para sentencias True
not 10 % 3 <= 10 % 2 # Regresa True
not not False # Regresa False
''' Cuando se combinan, los operadores booleanos arrojan los siguientes resultados:
True and True es True
True and False es False
False and True es False
False and False es False
True or True es True
True or False es True
False or True es True
False or False es False
Not True es False
Not False es True '''
False or not True and True # regresa False
False and not True or True # Regresa True
True and not (False or False) # Regresa True
not not True or False and not True # Regresa True
False or not (True and True) # Regresa False

# Sentencias condiconales
''' Existen tres sentencias condicionales '''
# if. Evalúa que una expresión sea o no cierta y ejecuta un bloque de código en base a ello
def una_funcion ():
	if 2 == 1 + 1: # Si la expresión es cierta...
		return True # ... la función regresa un True
# else. Si la expresión evaluda por un if no resulta en True, se usa un else
def una_funcion ():
	if 2 == 1: # Si la expresión es cierta...
		return True # ... la función regresa un True...
	else: #... de otra forma...
		return False # ... regresa un False
#elif. Sirve para manejar diferentes opciones
answer = '¡Me gusta el jamón!'
def una_funcion ():
	if answer == '¡Me gusta el jamón!': # Si la expresión es cierta...
		return True # ... la función regresa un True
	elif answer == '¡Odio el jamón!': # Si la expresión anterior no resultó cierta...
		return False # ... la función regresa False
	else: # De cualquier otra forma...
		print '¿Entonces qué diablos te gusta?' # ... la función regresa False