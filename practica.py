# Determinar si un número es par
def es_par (i):
	return i % 2 == 0 # Se determina si un número es par cuando al dividirlo entre 2 no deja residuo

# Determinar si un número es entero
def es_entero (i):
	return abs(i) == abs(round(i)) # Se determina si un número es entero (independientemenet de que sea de tipo int o float) comparando su valor absoluto contra su valor absoluto una vez redondeado

# Sumar todos los dígitos de un número
def suma_de_digitos(n): # Se asume que el número recibido es siempre positivo
	suma = 0 # La suma inicial es de 0
	while n > 0: # Mientras "n" sea mayor a 0...
		suma += n % 10 # ... se suma el valor del último dígito (el residuo de dividirlo entre 10)...
		n //= 10 # ... y se le asigna un nuevo valor a "n" dividiéndolo y redondeándolo entre 10 (lo que quita el último dígito)
	return suma # Se obtiene la suma de todos los dígitos

# Facorial
''' El factorial de un entero no negativo "x" es igual al producto de todos los enteros menores o iguales a "x" y mayores a 0 '''
def factorial (n):
	factorial = n # El factorial se inicializa en el valor de n
	while n > 1: # Mientras "n" sea mayor a 1...
		factorial *= n - 1 # ... el factorial es resultado de multiplicarse a sí mismo por "n" menos 1
		n -= 1 # Se resta 1 a "n"
	return factorial # Se obtiene el factorial

# Determinar si un número es primo
''' Un número primo es un número entero positivo mayor que 1 que no tiene divisores positivos distintos a 1 y a sí mismo '''
def es_primo (n):
	if n < 2: # Si el número es menor a 2...
		return False # ... la función regresa False (no hay números primos menores a 2)
	else: # De otra forma...
		for i in range (2, n): # ... se itera en la serie 2 a "n"...
			if n % i == 0: # ... y si un número intermedio resulta ser un divisor...
				return False # ... la función regresa False
		else: # Si el bucle terminó y no encontró números intermedios divisores...
			return True # ... la función regresa True

# Strings en sentido inverso
''' Una lista puede ordenarse en sentido inverso usando el método reverse(). Para strings debe hacerse de forma manual '''
def reverse (string):
	nuevo_string = '' # Se declara un string vacío
	for i in range(len(texto), 0, -1): # Se itera en sentido inverso sobre el rango de longitud del string y 0
		nuevo_string += string[i - 1] # Se va concatenando cada caracter en sentido inverso al nuevo string
	return nuevo_string

# Censor de palabras
def censor (text, word): # Se declara la función para censurar palabras. Recibe un texto y la palabra a censurar
	words = text.split() # Aplicando el método split a un string, se obtiene una lista con todas las palabras de ese string (divide el string al encontrar espacios en blanco)
	for i in range(0, len(words)): # Se itera sobre los índices de la nueva lista
		if words[i] == word: # Si el elemento de la lista es igual a la palabra a censurar...
			words[i] = '*' * len(word) # ... se sustituye el elemento por asteríscos (el número de asteriscos coincide con el número de caracteres de la palabra)
	return ' '.join(words) # Mediante el método join, se unen todos los elementos de la lista, en este caso separados por un espacio en blanco, es decir, se aplica la acción contraria a split()
censor('we\'ve got fucking faith', 'fucking')

# Contar dentro de una lista
def contador (secuencia, elemento):
	return secuencia.count(elemento) # El método count() aplicado a una lista recibe un argumento correspondiente a un elemento a buscar y regresa el número de veces que dicho elemento se encuentra repetido en la lista