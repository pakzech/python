# Bit
''' En todos los ordenadoreslos números se representan con bits, una serie de números binarios que comprenden ceros y unos
Humanamente contamos en base decimal, lo que quiere decir que cada dígito puede contener uno de 10 valores: 0 a 9
En el sistema binario se cuenta en base a 2, lo que quiere decir que cada dígito puede tener un valor de 0 o 1
El patrón de conteo es el mismo. Debe hacerse un acarreo de columna cada vez que un número es mayor a 1, a diferencia del sistema decimal, donde los acarreos de columna se hacen cuando un número es mayor a 9
En el sistema decimal cada dígito representa una potencia de 10; en el binario cada dígito representa una potencia de 2 (un bit)
El bit más a la derecha es el primero (dos a la cero), el siguiente es el segundo (dos a la uno), y así sucesivamente: 4, 8, 16, 32, 64, etc.
Así, por ejemplo, 1010 equivale a 10:
8 bit	4 bit	2 bit	1 bit
   1	   0	   1	   0
   8 	   0	   2 	   0	= 10
En Python puede escribirse en sistema binario comenzando con 0b '''
0b0 # Longitud de 1 bit. 1 bit * 0 = 0
0b1 # Longitud de 1 bit. 1 bit * 1 = 1
0b10 # Longitud de 2 bit. 2 bit * 1 = 2
0b11 # Longitud de 2 bit. (2 bit * 1) + (1 bit * 1) = 3
0b100 # Longitud de 4 bit. 4 bit * 1 = 4
0b101 # Longitud de 4 bit. (4 bit * 1) + (1 bit * 1) = 5
0b110 # Longitud de 4 bit. (4 bit * 1) + (2 bit * 1) = 6
0b111 # Longitud de 4 bit. (4 bit * 1) + (2 bit * 1) + (1 bit * 1) = 7
0b1000 # Longitud de 8 bit. 8 bit * 1 = 8
0b1001 # Longitud de 8 bit. (8 bit * 1) + (1 bit * 1) = 9
0b1010 # Longitud de 8 bit. (8 bit * 1) + (2 bit * 1) = 10

# bin()
''' La función bin() toma como argumento un entero y regresa su valor en binario pero en formato de string, por lo que no una vez convertido no se puede operar en él como un valor numérico. También pueden representarse números en base octal o hexadecimal con oct() y hex() respectivamente '''
bin(10) # Regresa '1010'

# int()
''' La función int() toma un valor numérico no entero en un entero numérico '''
int('42') # Recibe un string y regresa un 42 numérico
int(50.2) # Recibe un real y regresa el entero redondeado hacia abajo, es decir 50
int('1010', 2) # Cuando se pasan dos argumentos el primero debe ser un string que contenga un número, el segundo un entero que represente la base en la cual se encuentra. Regresa el valor numérico apropiado, en este caso 10

# Desplazamiento de bits
''' Los operadores de desplazamiento funcionan desplazando los bits de un número determinado número de posiciones. Sólo pueden realizarse operaciones de desplazamiento en enteros. Cualquier otro valor arrojará una salida no lógica
<< Desplaza hacia la derecha las posiciones indicadas (equivale a multiplicar un número por 2 determinado número de veces)
>> Desplaza hacia la izquierda las posiciones indicadas (equivale a dividir un número entre 2 determinado número de veces) '''
0b000001 << 2 # Es igual a 0b000100 (1 << 2 = 4)
0b000101 << 3 # Es igual a (5 << 3 = 40)
0b0010100 >> 3 # Es igual a = 0b0000010 (20 >> 3 = 2)
0b0000010 >> 2 # Es igual a = 0b0000000 (2 >> 2 = 0)

# Operador & (Y)
''' El operador bit a bit & compara dos números a nivel de bits y regresa un número. En dicho número sus bits estarán encendidos (igual a 1) en caso de que los bits correspondientes de ambos números sean igual a 1. El resultado siempre es un número menor o igual al mayor de los dos valores
Cada bit en ambos números arroja:
0 & 0 = 0
0 & 1 = 0
1 & 0 = 0
1 & 1 = 1 '''
i = 0b00101010 # 42
j = 0b00001111 # 15
i & j # Regresa 00001010 (10). En esta comparación los únicos bits que coinciden por estar encendidos en 1 en ambos números son los de 8 y 2 tanto en i como en j. Por lo tanto i & j contiene solo esos bits en 1 y todos los demás son pasados a 0
0b111 & 0b1010 # 7 & 10 regresa 0b10, lo que equivale a 2

# Operador | (O)
''' El operador bit a bit | compara dos números a nivel de bits y regresa un número. En dicho número sus bits estarán encendidos (igual a 1) en caso de que los bits correspondientes a uno de los números sea igual a 1. El resultado siempre es un número mayor o igual a las dos entradas numéricas
Cada bit en ambos números arroja:
0 | 0 = 0
0 | 1 = 1
1 | 0 = 1
1 | 1 = 1 '''
i = 0b00101010 # 42
j = 0b00001111 # 15
i | j # 42 | 15 regresa 0b101111, lo que equivale a 47

# Operador ^ (XOR)
''' El operador bit a bit ^ (disyunción exclusiva) compara dos números a nivel de bits y regresa un número. En dicho número sus bits estarán encendidos (igual a 1) en caso de que los bits correspondientes a cualquiera de los dos números sea igual a 1, más no en ambos. Cuando se emplea una disyunción exclusiva sobre un mismo número el resultado será siempre 0
Cada bit en ambos números arroja:
0 ^ 0 = 0
0 ^ 1 = 1
1 ^ 0 = 1
1 ^ 1 = 0 '''
i = 0b00101010 # 42
j = 0b00001111 # 15
i ^ j # 42 ^ 15 regresa 0b100101, lo que equivale a 37

# Operador ~ (NO)
''' El operador bit a bit ~ invierte todos los bits de un número entero, lo cual resulta en una operación muy compleja, aunque matemáticamente equivale a añadir 1 al número y hacerlo negativo '''
~ 1 # Regresa -2
~ 0b10 # Regresa -3

# Máscaras de bits
''' Una máscara de bits es una variable ayuda a encender bits específicos, apagar otros o recoger datos acerca de qué bits están encendidos o apagados en un entero '''
a = 0b110 # 6
mascara = 0b100 # 4
deseado = a & mascara # Revisa si el tercer bit, comenzando desde la derecha, en a está encendido. Utilizando el operador & con 0b100 resultará que todos los bits de 15 se apagarán, a excepción del tercer bit. Si el número que se obtiene es mayor a 0, entonces dicho bit está encendido. Si no, el bit está apagado desde el principio
mascara = 0b1 # 2
deseado = a | mascara # Revisa si el primer bit, comenzando desde la derecha, en a está encendido y si no lo está entonces lo enciende
mascara = 0b111 # 7
deseado = a ^ mascara # Invierte los primeros tres bits, comenzando desde la derecha, en a
a = 0b101 # 5
mascara = (0b1 << 9) # 512 o 0b1000000000. Posiciona en bit 1 en 1 y luego lo desplaza nueve veces con lo que se obtiene 512 o 0b1000000000 en binario
deseado = a | mascara