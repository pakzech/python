﻿# Listas
''' Las listas son un tipo de datos que sirve para almacenar una colección de diferentes fragmentos de información en forma de secuencia bajo un solo nombre de variable '''
zoo_animals = ['pangolín', 'casuario', 'cocodrilo'] # Las listas se declaran entre [] y con los valores individuales separados por comas

# Acceso mediante índices
''' Puede accederse a los elementos de una lista de forma individual mediante su índice, es decir, mediante su posición dentro de la lista. Los índices comienzan a contarse a partir del 0 '''
numeros = [5, 6, 7, 8] # Se declara una lista
print numeros[0] # Se imprime el valor del primer elemento
print numeros[3] # Se imprime el valor del cuarto elemento

# Modificación mediante índices
''' Además de acceder a los valores de una lista mediante índices, también es posible reasignarlos '''
zoo_animals = ['pangolín', 'casuario', 'cocodrilo']
zoo_animals[1] = 'gallina' # Se reasigna el índice 1 y se cambia casuario por gallina

# Agregar elementos a una lista
''' Es posible agregar nuevos elementos a una lista mediante el método append() '''
maletin = [] # Se declara una lista vacía
maletin.append('cigarros') # Mediante el método append() se agrega un nuevo elemento al final de la lista
maletin.append('gafas') # Se agrega un elemento más
print maletin # Ahora la lista maletin contiene 2 elementos

# Particionado de listas
''' Una lista se particiona mediante slice, sin modificar la lista original '''
maletin = ['gafas', 'sombrero', 'pasaporte', 'portátil', 'traje', 'zapatos'] # Una lista de 6 elementos
primeros = maletin[0:2] # maletin[0:2] regresa una nueva lista que contiene los elementos correspondientes desde el índice 0 hasta antes de comenzar el índice 2, es decir, los primeros 2 elementos
mitad = maletin[2:4] # maletin[2:4] regresa una nueva lista con los elementos correspondientes a partir del índice 2 y hasta antes de comenzar el 4
ultimos = maletin[-2:] # maletin[-2:] regresa una nueva lista comenzando a partir del índice -2 (cuando un índe contiene números negativos, comienza a contarse desde el final y hacia atrás), es decir, desde el penúltimo hasta el último

# Particionado de strings
''' Es posible particionar strings de la misma forma que se hace con una lista. De hecho puede pensarse en los strings como listas de caracteres '''
animales = 'catdogfrog' # Se declara un strings
cat = animales[:3] # Se obtienen los primeros 3 caracteres del string indicado
dog = animales[3:6] # Se obtienen los acarteres correspondientes desde el índice 3 y hasta antes de comenzar en 6
frog = animales[6:] # Se otienen los caracteres restantes a partir del índice 6 y hasta el último

# Búsqueda de índices e insertado
''' El método index() regresa el índice de un substring (en el caso de strings) o el de un elemento (en el caso de listas). El métod insert() puede insertar elementos intermedios en una lista '''
animales = ['cerdo hormiguero', 'tejón', 'pato', 'emu', 'zorro del desierto']
indice_pato = animales.index('pato') # El método index() recibe un argumento, en este caso un valor. Si ese valor pertenece a un elemento de la lista, regresará el número de índice al que pertenece. Se almacena e una variable
animales.insert(indice_pato, 'cobra') # El método insert recibe dos argumentos, el primero es un número de índice, el segundo es un valor a insertar en ese índice. Cuando se inserta un valor e un índice intermedio, todos los elementos que se encentren después del nuevo elemento recorrerán su índice hacia adelante
print animales # Al imprimir la lista, ahora cobra forma parte de la lista y está ubicado entre tejón y pato

# Iterar sobre una lista
''' Mediante for es posible iterar sobre una lista (o un string). El bucle for pasa por cada elemento de una lista o diccionario pero sin ningún orden particular '''
mi_lista = [1, 9, 3, 8, 5, 7] # Se declara una lista que contiene valores numéricos
for numero in mi_lista: # Para iterar sobre los valores de una lista se usa for, seguido de un nombre de variable temporal que corresponde a cada elemento; in indica la lista sobre la que se iterará. Se termina con:
	print numero * 2 # Se imprime el valor del elemento multiplicado por 2. La traducción del for es: por cada elemento en la lista, imprime el valor del elemento por 2
ivan = 'Ivan Pacheco' # Se declara un string
for letra in ivan: # Los bucles for también sirven para iterar en strings, en este caso sobre cada caracter del string
	print letra

# Ordenar listas
''' El método sort() reordena los valores dentro de una lista de forma numérica/alfabética ascendente. El método sort() no regresa una nueva lista con los elementos ordenados, sino que modifica directamente la lista '''
mi_lista = [1, 9, 3, 8, 5, 7] # Se declara una lista que contiene valores numéricos
mi_lista.sort() # Se modifica la lista para ordenar los elementos de forma ascendente
print mi_lista

# Diccionarios
''' Los diccionarios son parecidos a las listas, sólo que se puede acceder a ello s través de claves y no de índices '''
residentes = {'Frailecillo': 104, 'Peresozo': 105, 'Pitón de Birmania': 106} # A diferencia de las listas, los diccionarios se delimitan con {} y no con []. Los elementos se separan igualmente con comas
print residentes['Frailecillo'] # Se accede al valor contenido en la llave Frailecillo, es decir 104

# Nuevas entradas
''' Cuando se asigna una nueva clave, se crea un par clave-valor. '''
nuevo_diccionario = {} # Se crea un diccionario vacío
nuevo_diccionario['Agua'] = 10 # Se agrega un nuevo par al diccionario
nuevo_diccionario['Lista'] = [2, 3, 4] # Se agrega otro par
len(nuevo_diccionario) # Regresa 2
print nuevo_diccionario # Imprime el diccionario

# Eliminar pares
''' Los diccionarios, al igual que las listas, son mutables '''
nuevo_diccionario = {'Agua': 10, 'Jugo': 'Hecho de naranja'}
nuevo_diccionario['Agua'] = 15 # Puede modificarse el valor de una clave con sólo reasignarla
del nuevo_diccionario['Agua'] # Con el comando del se elimina el par de la clave especificada
nuevo_diccionario.remove('Hecho de naranja') # El método remove() hace lo mismo que el comando del, sin embargo éste elimina el primer par que contiene el valor indicado