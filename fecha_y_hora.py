﻿# Para trabajar con fecha y hora hay que importar la librería indicada
from datetime import datetime
now = datetime.now() # En una variable se almacena un objeto que contiene la fecha y hora actuales
print '%i/%i/%i %i:%i:%i' % (now.month, now.day, now.year, now.hour, now.minute, now.second) # Se imprime la fecha y hora actuales (%i significa integrer)
'''
now.month obtiene el mes actual
now.day obtiene el día actual
now.year obtiene el año actual
now.hour obtiene la hora actual
now.minute obtiene el minuto actual
now.second obtiene el segundo actual
'''